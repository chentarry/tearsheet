//fetch all the images
var rawNodes = document.getElementsByTagName("img");

//generate payload
var images = [];

//just need link URLs
for(var i = 0; i < rawNodes.length; i++){
    var imageLocation = rawNodes[i].attributes.src.value;

    //if no protocal, assume they're using shorthand
    if(imageLocation.indexOf(window.location.protocol) < 0){
        //however, if shorthand has first character being "/", it's an absolute path
        if(imageLocation[0] == "/")
        {
            imageLocation =  window.location.protocol + 
                        "//" + window.location.host +
                        imageLocation;
        }
        else //add the relative path location
        {
            var pathName = window.location.pathname.substr(0, window.location.pathname.lastIndexOf("/"));
            imageLocation = window.location.protocol + 
                        "//" + window.location.host  + pathName + "/" + imageLocation;
        }
    }

    images.push(imageLocation)
}

//pass it up to the application
chrome.runtime.sendMessage({"collection": images}, function(response) {
  console.log(response.message);
});