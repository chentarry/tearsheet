/**
Main application js file.
@module tearsheet
@namespace com_lifein50mm_tearsheet
@class App
@main App
*/
(function(com_lifein50mm_tearsheet,$,undefined){

    /**
        Property that has the current images the user polled.
        @property CurrentImages
        @private
        @type Object
    */
    var CurrentImages = {};

    /**
        Property that determines whether or not user wants to select and 
        tag multiple images at once.
    **/
    var MultiSelectionMode = false;

    var Init = function(){
        //bind content script event listener
        chrome.runtime.onMessage.addListener(ImageEventListener);

        //inject content script again
        InjectContentScript();
    };


    var InjectContentScript = function(tab){
        chrome.tabs.executeScript(null, {file:"js/jquery-2.0.0.min.js"}, function(){
            chrome.tabs.executeScript(null, {file:"js/content.js"});
        });
    };

    var ImageEventListener = function(request, sender, sendResponse){
        console.log("received message!");

        CurrentImages = request.collection;
        console.log(request);
        console.log(CurrentImages);

        AppendImages();
        sendResponse = function(){return {message:"Success!"}};
        //send response
        return true;
    };

    var AppendImages = function(){
        WipeImages();

        //insert new ones
        for (var i = 0; i < CurrentImages.length; i++) {
            var imageTag = document.createElement("img");
            imageTag.setAttribute("src", CurrentImages[i]);
            imageTag.setAttribute("alt", "image" + i);
            imageTag.setAttribute("class", "img-polaroid scrape-image");
            $("#pictures").append(imageTag);
        };
    }

    ///helper functions
    var NavigationControl = function(classToHighlight){
        $(".active").removeClass("active");
        $("."+classToHighlight).addClass("active");
        return;
    }

    var WipeImages = function(){
        //wipe out current images
        $("#pictures").text("");
    }

    var ToggleSelected = function(imageToToggle)
    {
        var targetImage = $(imageToToggle);
        if(targetImage.hasClass("unselected"))
        {
            targetImage.removeClass("unselected");
        }
        else
        {
            targetImage.addClass("unselected");
        }
    }

    ///event handlers

    //event handlers for the right hand side menu
    /*
    *  On click, injects the content script that scrapes that page's images
    *  and puts them in the extensions for the user to pick from.
    */
    var ScrapeClick = function(e){
        //if already selected, return
        if($(".scrape").hasClass("active")){
            return;
        }

        //hide drag and drop section
        $("#drag").hide();

        //toggle navigation
        NavigationControl("scrape");

        //inject content script again
        InjectContentScript();
    }

    /*
    *  On click, renders the user's stored images from the FileSystem API.
    */
    var StoredClick = function(e){
        
        //hide drag and drop section
        $("#drag").hide();

        //wipe out current images
        WipeImages();

        //toggle navigation
        NavigationControl("stored");

        //TODO: fetch images from local storage API

    }

    var DraggedClick = function(e){
        //wipe out current images
        WipeImages();

        //show drag and drop section
        $("#drag").show();

        //toggle navigation
        NavigationControl("drag");
    }


    //event handlers for everything else
    var ScrapedImageClick = function(e){
        var target = e.target;
        
        //if multiselect, let them multiselect
        if(MultiSelectionMode)
        {
            ToggleSelected(target);    
        }

        //else, proceed through workflow
        //TODO, figure out what to do after user selects an image to tag
    }

    //call the init function
    Init();
    console.log("finished");

    //add event handlers
    $(document).on("click", ".scrape", ScrapeClick);
    $(document).on("click", ".stored", StoredClick);
    $(document).on("click", ".drag", DraggedClick);
    $(document).on("click.scrape", ".scrape-image", ScrapedImageClick);
}(com_lifein50mm_tearsheet = window.com_lifein50mm_tearsheet || {}, jQuery))
